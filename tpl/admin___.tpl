<!DOCTYPE html>
<html>
{block name="<html>"}
	<head>
	{block name="<head>"}
		{block name="meta"}
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		{/block}
		{block name="<script>"}
		<script src="library/jquery/jquery-2.2.4.js" crossorigin="anonymous"></script>
		<script src="library/bootstrap/js/bootstrap.min.js" crossorigin="anonymous"></script>
		<script src="library/admin/app.min.js" crossorigin="anonymous" async='true'></script>
		{/block}
		{block name="stylesheet"}
		<link rel="stylesheet" href="library/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">
		<link rel="stylesheet" href="library/bootstrap/css/font-awesome.min.css" crossorigin="anonymous">
		<link rel="stylesheet" href="library/admin/AdminLTE.css" crossorigin="anonymous">
		<link rel="stylesheet" href="library/admin/skin-blue.css" crossorigin="anonymous">
		{/block}
	{/block}
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
	{block name="<body>"}
	{/block}
	</body>
{/block}
<html>