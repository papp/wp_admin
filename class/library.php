<?
class wp_admin__class__library extends wp_admin__class__library__parent
{
	function __construct(&$D = null)
	{
		parent::{__function__}($D);
		#$D['CL']->setting()->get_setting($D);
		
		
		$this->D['LIBRARY']['JS']['ADMIN']		= ['ACTIVE'	=> 1, 'REQUIRED' => 'BOOTSTRAP', 'FILE' => ['library/admin/app.min.js'] ];
		$this->D['LIBRARY']['CSS']['ADMIN']		= ['ACTIVE'	=> 1, 'REQUIRED' => 'BOOTSTRAP', 'FILE' => ['library/admin/AdminLTE.css','library/admin/skin-blue.css']];

	}
} 