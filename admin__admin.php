<?
class wp_admin__admin__admin extends wp_admin__admin__admin__parent
{
	function load($d=null)
	{
		parent::{__function__}();
		$this->C->user()->check_right(['RIGHT'=>'ADMIN']);
		#ToDo: Navigation aus der Settings laden
		#$this->C->library()->smarty()->assign('D', $this->D);
		#$this->C->library()->smarty()->display(__dir__.'/tpl/admin.tpl');
	}
	
	function show($d=null)
	{
		$this->D['TEMPLATE']['FILE'][] = basename(__file__,'.php').".tpl";
		parent::{__function__}();
		$this->C->template()->display();
	}
}