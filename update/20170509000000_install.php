<?
class wp_admin__20170509000000_install
{
	function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; }
	function __call($m, $a){ return $a[0]; } 
	
	function up()
	{
		$this->D['USER']['D']['admin'] = [
			'ACTIVE'	=> '1',
			'NICK'		=> 'admin',
			'PASSWORD'	=> $this->D['CONFIG']['D']['ROOT_PASSWORD'],
		];
		$this->D['MODUL']['D']['wp_user']['USER']['D']['admin'] = [
			'ACTIVE'	=> '1',
			'NICK'		=> 'admin',
			'PASSWORD'	=> $this->D['CONFIG']['D']['ROOT_PASSWORD'],
		];
		$this->C->user()->set_user();
		
		$this->D['MODUL']['D']['wp_seo']['SEO']['D']['admin__admin'] = [
			'ACTIVE'	=> '1',
			'SEO_URL'	=> 'admin',
			'URL'		=> 'D[PAGE]=admin__admin',
		];
		$this->C->seo()->set_url();
		return 1;
	}
	
	function down()
	{
		$this->D['USER']['D']['admin'] = ['ACTIVE'	=> '-1'];
		$this->D['MODUL']['D']['wp_user']['USER']['D']['admin'] = ['ACTIVE'	=> '-1'];
		$this->C->user()->set_user();
		
		$this->D['MODUL']['D']['wp_seo']['SEO']['D']['admin__admin'] = ['ACTIVE'	=> '-1'];
		$this->C->seo()->set_url();
		return 1;
	}
}