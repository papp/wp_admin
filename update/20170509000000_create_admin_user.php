<?
class wp_admin__20170509000000_create_admin_user
{
	function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; }
	function __call($m, $a){ return $a[0]; } 
	
	function up()
	{
		$this->D['USER']['D']['admin'] = [
			'ACTIVE'	=> '1',
			'NICK'		=> 'admin',
			'PASSWORD'	=> $this->D['CONFIG']['D']['ROOT_PASSWORD'],
		];
		$this->D['MODUL']['D']['wp_user']['USER']['D']['admin'] = [
			'ACTIVE'	=> '1',
			'NICK'		=> 'admin',
			'PASSWORD'	=> $this->D['CONFIG']['D']['ROOT_PASSWORD'],
		];
		$this->C->user()->set_user();
		
		return 1;
	}
	
	function down()
	{
		$this->D['USER']['D']['admin'] = [
			'ACTIVE'	=> '-1',
			'NICK'		=> 'admin',
			'PASSWORD'	=> $this->D['CONFIG']['D']['ROOT_PASSWORD'],
		];
		$this->D['MODUL']['D']['wp_user']['USER']['D']['admin'] = [
			'ACTIVE'	=> '-1',
			'NICK'		=> 'admin',
			'PASSWORD'	=> $this->D['CONFIG']['D']['ROOT_PASSWORD'],
		];
		$this->C->user()->set_user();
		return 1;
	}
}